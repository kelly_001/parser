﻿<?php
require_once 'simple_html_dom.php';
require_once 'csv.php';

function getPage($url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    if (!($data = curl_exec($curl)))
        //echo "page: ".$data;
        echo "error" . curl_error($curl);
    curl_close($curl);
    return $data;
}

function parsingJson($page)
{
    $result = array();
    $pattern = "/\[\"[0-9]+\",\"[0-9]+\",/";
    $pattern1 = "/\"[0-9]+\"/";
    preg_match_all($pattern, $page, $matches, PREG_SET_ORDER);
    if (count($matches)) {
        foreach ($matches as $match) {
            preg_match_all("/[0-9]+/", $match[0], $id, PREG_SET_ORDER);
            $result[] = $id[1][0];
        }
    }
    return $result;

}

function getFilters($str)
{
    $filters = array();
    // загружаем данный URL
    $data = str_get_html($str);

    // очищаем страницу от лишних данных, это не обязательно, но когда HTML сильно захламлен бывает удобно почистить его, для дальнейшего анализа
    foreach ($data->find('script,link,comment') as $tmp) $tmp->outertext = '';

    if (count($data->find("select#filtr option[value!='']"))) {

        foreach ($data->find("select#filtr option[value!='']") as $filter) {
            array_push($filters, $filter->value);
        }
    } else echo "not found";
    $data->clear(); // подчищаем за собой
    unset($data);
    return $filters;
}

function getProducts($str, $array, $split_options = "no")
{
    $product = array();
    $data = str_get_html($str);
    $default_keys = array("Цена прайс", "Артикул");
    //$keys = array("Описание", "Применение");
    $keys = $array;
    $result_values = array();
    $description = "";

    if (count($data->find("div#catalog_ajax"))) {
        if (count($data->find("div#catalog_ajax div", 0)->find('span'))) {
            $name = $data->find("div#catalog_ajax span[title!='']", 0)->title;
        } else {
            //foreach($data->find("div#catalog_ajax div",0)->find("button") as $button ) $button->outertext = '';
            $name = $data->find("div#catalog_ajax div", 0)->plaintext;
            $name = preg_replace(array("/В каталог/", "/Сравнить/", "/закладки/"), "", $name);
        }
        $result_values[] = $name;
        //echo "Name: ". $name. "<br>";
        $photo = $data->find("div#catalog_ajax img[src^='http://']", 0)->src;
        $result_values[] = $photo;
        //echo "Photo: ".$photo."<br />";
        // making mini photo ".min140.jpg"
        $mini_photo = preg_replace("/.jpg/", ".min140.jpg", $photo);
        $result_values[] = $mini_photo;
        //echo $mini_photo."<br />";
        //Цена СП
        $priceSP = $data->find("div#catalog_ajax table table tr td", 0)->plaintext;
        $priceSP = preg_replace("/руб/", "", $priceSP);
        $result_values[] = $priceSP;
        //echo $priceSP."<br />";
        //таблица - артикул, цены, категория, описание
        //content from span from tables td
        $spans = array();
        if (count($data->find("div#catalog_ajax table span"))) {
            foreach ($data->find("div#catalog_ajax table span[title!='']") as $span) {
                $spans[] = $span->title;
            }
        }

        if (count($data->find("div#catalog_ajax table table tr td"))) {
            foreach ($default_keys as $key) {
                foreach ($data->find("div#catalog_ajax table table tr td") as $td) {
                    if (preg_match('/' . $key . '/', $td->plaintext)) {
                        $td = $td->next_sibling();
                        $result_values[] = $td->plaintext;
                    }
                }
            }
        }

        if (isset($keys)){
            if (count($data->find("div#catalog_ajax table table tr td"))) {
                foreach ($keys as $key) {
                    $value = "";
                    $flag = -5;
                    $td_i = 0;
                    foreach ($data->find("div#catalog_ajax table table tr td") as $td) {
                        if (preg_match('/' . $key . '/', $td->plaintext) || (($td_i === $flag + 2) && preg_match('/\-:/', $td->plaintext))) {
                            if ($td->next_sibling() !== null) {
                                $match = $td->next_sibling()->plaintext;
                                //echo "match ".$td_i.":".$match.""."<br />";
                                foreach ($spans as $span) {
                                    if (strpos($span, substr($match, 0, 15)) === 0) $match = $span;
                                }
                                $value .= trim($match);
                                $flag = $td_i;

                            }
                        };
                        $td_i += 1;
                    }
                    //
                    if ($split_options == "no") $result_values[] = $value;
                        else $description .= trim($key . ": " . $value);
                }
            }
        }
        if ($split_options != "no") $result_values[] = $description;
    } else echo "not found";
    $data->clear(); // подчищаем за собой
    unset($data);
    //$product = $result_values;
    return $result_values;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $delimiter = ";";
    $split_options = "no";
    if (isset ($_POST['delimiter'])) $delimiter = $_POST['delimiter'];
    if (isset ($_POST['url'])) $catalog_num = $_POST['url'];
    //if (isset ($_POST['file'])) $file = $_POST['file'];
    if (isset ($_POST['type'])) $type = $_POST['type'];
    if (isset ($_POST['id'])) $post_id = $_POST['id'];
    if (isset ($_POST['fields'])) $post_fields = $_POST['fields'];
    if (isset ($_POST['options'])) $split_options = $_POST['options'];
    $format = ".csv";
    $ajax_url = "http://sp.38mama.ru/sp/catalog/ajax?brendId=" . $catalog_num;
    $url = "http://sp.38mama.ru/sp/catalog/index/brendId/" . $catalog_num;
    //$product_url = "http://sp.38mama.ru/sp/newCatalog/index/id/".$product_id;
    //$url = getPage($url);
    if ($type === 'filter') {
        $url = getPage($url);
        $filters = getFilters($url);
        //var_dump($filters);
        $parser = new CsvWriter("files/filter_" . $catalog_num.$format, array($filters), $delimiter);
        $parser->GetCsv();
    } else if ($type === 'catalog') {
        $page = getPage($ajax_url);
        //$page = json_decode($page); //декодирование json строки в нормальный вид с русскими буквами и слешами
        $products = array();
        $products_id = parsingJson($page); //получаем список ид всех товаров
        if (count($products_id)) {
            //сохраняем ид
            $parser_id = new CsvWriter("files/id_" . $catalog_num . $format, array($products_id), $delimiter);
            $parser_id->GetCsv();
            $page = getPage($ajax_url);
            $products = array();
            //$products[] = array("ID", "Наименование", "Мини фото", "Фото", "Цена СП", "Цена прайс", "Артикул", "Описание");
            $products[] = array("ID", "Наименование", "Мини фото", "Фото", "Цена СП", "Цена прайс", "Артикул");
            if ($split_options !== "no") {
                $products[0][] = "Описание";
            } else {
                foreach($post_fields as $field) {
                    $products[0][] = $field;
                }
            }
            $parser = new CsvWriter("files/catalog_" . $catalog_num . $format, $products, $delimiter);
            $parser->GetCsv();
            //parsingJson to get products id
            $i = 0;
            foreach ($products_id as $id) {
                //parsing $product_url
                $product_url = "http://sp.38mama.ru/sp/newCatalog/index/id/" . $id;
                $product_page = getPage($product_url);
                $product = array($id);
                $products_fields = getProducts($product_page, $post_fields, $split_options);
                if (count($products_fields)) {
                    foreach ($products_fields as $field) {
                        if (strpos($field, $delimiter) === false) {
                            if ($field == "") $product[] = "Пустое значение";
                            else $product[] = $field;
                        } else {
                            echo "Error! В тексте закупки найден разделитель. Задайте другое значение для разделителя.";
                            echo "<a href='index.html'>Назад</a>";
                            exit;
                        }
                    }

                }
                //echo "<br/> PRODUCT ".$i.": ";
                //print_r($product);
                $products[] = $product;
                $parser->UpdateCsv(array($product));
                unset($product);
                $i++;
                echo("Товар ".$i." загружен, запись обновлена<br />");
                //if ($i === 5) break;
            }
            //print_r($products);
        }
    } else if ($type === "product") {
        $product_url = "http://sp.38mama.ru/sp/newCatalog/index/id/" . $post_id;
        $product_page = getPage($product_url);
        //$product_page = "files/".$post_id.".html";
        $product = array(array("ID","Наименование", "Мини фото", "Фото", "Цена СП", "Цена прайс", "Артикул"));
        if ($split_options !== "no") {
            $product[0][] = "Описание";
        } else {
            foreach($post_fields as $field) {
                $product[0][] = $field;
            }
        }
        $fields = getProducts($product_page, $post_fields, $split_options);
        $fields_value = array($post_id);
        if (count($fields)) {
            foreach ($fields as $field) {
                if (strpos($field, $delimiter) === false) {
                    if ($field == "") $fields_value[] = "Пустое значение";
                        else $fields_value[] = $field;
                } else {
                    echo "Error! В тексте закупки найден разделитель. Задайте другое значение для разделителя.";
                    echo "<a href='index.html'>Назад</a>";
                    exit;
                }
            }
        }
        $product[] = $fields_value;
        $parser_product = new CsvWriter("files/product_" . $post_id . $format, $product, $delimiter);
        print_r($product);
        $parser_product->GetCsv();
        //print_r($product);
    }
    echo("Загрузка завершена, см. файлы в каталоге files.");
}

exit;
?>



